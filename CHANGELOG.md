# Changelog

## 0.3

- [#1](https://gitlab.com/meltano/files-airflow/-/issues/1) Immediately create DAG for every scheduled pipeline, without waiting until a manual run has succeeded
